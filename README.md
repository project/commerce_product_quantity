-- SUMMARY --
A Drupal 9 module to validate for Product quantity limitation on checkout page.
The module is also support to validate multiple product's quantity.
For a full description of the module, visit the project page:
To submit bug reports and feature suggestions, or to track changes:

-- REQUIREMENTS --
None

-- INSTALLATION --

Install as usual, see http://drupal.org/node/1897420 for further information.

-- CONFIGURATION --

Select the product and set the quantity you want to allow to purchase per order. 
Configuration Available at "/admin/config/system/commerce_product_quantity"


-- CONTACT --
Current maintainers:

Harshit (Kudosintech) - https://www.drupal.org/u/harshit97
Dimple (Kudosintech) - https://www.drupal.org/u/dimplel
