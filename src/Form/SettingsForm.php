<?php

namespace Drupal\commerce_product_quantity\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure commerce_product_quantity settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'commerce_product_quantity_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['commerce_product_quantity.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('commerce_product_quantity.settings');

    /* Fetch All Products */
    $result = \Drupal::entityQuery('commerce_product')
      ->execute();
    $titles = ['none' => "- Select Products -"];

    foreach ($result as $product_id) {
      $entity_manager = \Drupal::entityTypeManager();
      $product = $entity_manager->getStorage('commerce_product')->load($product_id);
      if (isset($product)) {
        $titles[$product->product_id->value] = $product->get('title')->value;
      }
    }
    $config_count = $config->get('product_quantity_count');
    $field_count = $form_state->get('num_names');

    if ($config_count > 0 && empty($field_count)) {
      $field_count = $config->get('product_quantity_count');
      $form_state->set('num_names', $field_count);
    }

    $form['#tree'] = TRUE;

    $form['product_quantity_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Products With Total Allowed Quantity'),
      '#attributes' => ['id' => 'product-fieldset-wrapper'],
    ];

    if (empty($field_count)) {
      $form_state->set('num_names', 1);
      $field_count = 1;
    }

    for ($i = 0; $i < $field_count; $i++) {

      $form['product_quantity_fieldset'][$i] = [
        '#type' => 'fieldset',
        '#title' => '',
        '#attributes' => ['id' => 'product-fieldset-wrapper-child'],
      ];

      $form['product_quantity_fieldset'][$i]['product_quantity_fieldset']['product'] = [
        '#type' => 'select',
        '#title' => t('Select Product'),
        '#options' => $titles,
        '#default_value' => $config->get('product_' . $i),
      ];

      $form['product_quantity_fieldset'][$i]['product_quantity_fieldset']['quantity'] = [
        '#type' => 'number',
        '#title' => $this->t('Select Quantity'),
        '#default_value' => $config->get('quantity_' . $i),
        '#attributes' => ['min' => '1'],
      ];
    }
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['product_quantity_fieldset']['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Product'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'product-fieldset-wrapper',
      ],
    ];
    if ($field_count > 1) {
      $form['product_quantity_fieldset']['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => ['::removeOne'],
        '#ajax' => [
          'callback' => '::removeCallback',
          'wrapper' => 'product-fieldset-wrapper',
        ],
      ];
    }
    $form_state->setCached(FALSE);
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    $form['#attached']['library'][] = 'commerce_product_quantity/commerce_product_quantity';
    return $form;
  }

  /**
   * Function to add field set count.
   *
   * @param array $form
   *   Object to manage form and increase count.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form State object to manage form.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $field_count = $form_state->get('num_names');
    $add_button = $field_count + 1;
    $form_state->set('num_names', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Function to remove field set count.
   *
   * @param array $form
   *   Object to manage form and increase count.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form State object to manage form.
   */
  public function removeOne(array &$form, FormStateInterface $form_state) {
    $field_count = $form_state->get('num_names');
    $add_button = $field_count - 1;
    $form_state->set('num_names', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Function to add field set count.
   *
   * @param array $form
   *   Object to manage form and increase count.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form State object to manage form.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['product_quantity_fieldset'];
  }

  /**
   * Function to remove field set count.
   *
   * @param array $form
   *   Object to manage form and increase count.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form State object to manage form.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    return $form['product_quantity_fieldset'];
  }

  /**
   * Function to validate form.
   *
   * @param array $form
   *   Object to manage form and increase count.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form State object to manage form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $product_quantities = $form_state->getValue('product_quantity_fieldset');
  }

  /**
   * Submit function handler to save configuration.
   *
   * @param array $form
   *   Object to manage form and increase count.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form State object to manage form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('commerce_product_quantity.settings');
    $product_quantities = $form_state->getValue('product_quantity_fieldset');
    $i = 0;
    $field_count = $form_state->get('num_names');
    $config->set('product_quantity_count', $field_count)->save();
    foreach ($product_quantities as $value) {
      $product_quantity = $value['product_quantity_fieldset'];
      if (!empty($product_quantity)) {
        foreach ($product_quantity as $key => $value2) {
          switch ($key) {
            case 'product':
              $config->set('product_' . $i, $value2)->save();
              break;

            case 'quantity':
              $config->set('quantity_' . $i, $value2)->save();
              break;
          }
        }
      }
      $i++;
    }
  }

}
