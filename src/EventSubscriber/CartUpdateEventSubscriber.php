<?php

namespace Drupal\commerce_product_quantity\EventSubscriber;

use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\Event\CartOrderItemUpdateEvent;
use Drupal\commerce_cart\Event\CartEvents;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Cart Event Subscriber.
 */
class CartUpdateEventSubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   The Cart Manager.
   */
  public function __construct(MessengerInterface $messenger, CartManagerInterface $cart_manager) {
    $this->messenger = $messenger;
    $this->cartManager = $cart_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      CartEvents::CART_ORDER_ITEM_UPDATE => [['updateInCart', -100]],
    ];
  }

  /**
   * Add a related product automatically.
   *
   * @param \Drupal\commerce_cart\Event\CartOrderItemUpdateEvent $event
   *   The cart add event.
   *
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public function updateInCart(CartOrderItemUpdateEvent $event) {

    $config = \Drupal::config('commerce_product_quantity.settings');
    $product_count = $config->get('product_quantity_count');
    $product_list = [];
    for ($i = 0; $i < $product_count; $i++) {
      $product_list[$i]['product'] = $config->get('product_' . $i);
      $product_list[$i]['quantity'] = $config->get('quantity_' . $i);
    }

    $cart = $event->getCart();
    $cart_items = $cart->getItems();

    foreach ($cart_items as $cart_item) {
      $quantity_flag = $number_item = $default_quantity = 0;
      $quantity = $cart_item->getQuantity();
      $temp_product_variation = $cart_item->getPurchasedEntity();
      $product_id = $temp_product_variation->product_id->getString();
      $product_key = array_search($product_id, array_column($product_list, 'product'));
      if (isset($product_key)) {
        $quantity_flag = $quantity_flag + $quantity;
        $number_item = $number_item + 1;
        $default_quantity = $product_list[$product_key]['quantity'];
      }
      if ($quantity_flag > $default_quantity) {
        if ($number_item == 1) {
          $cart_item->setQuantity($default_quantity);
          $this->messenger->deleteAll();
          $this->messenger->addMessage(t('You have reached max quantity for this product'), 'error');
        }
        else {
          $cart->removeItem($cart_item);
          $cart_item->delete();
          $this->messenger->deleteAll();
          $this->messenger->addMessage(t('You have reached max quantity for this product'), 'error');
        }
      }
    }
    $cart->save();
  }

}
